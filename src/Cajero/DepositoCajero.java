package Cajero;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import EDU.purdue.cs.bloat.decorate.Main;

import cajeros.ClientesCajero;
import cajeros.funcionescrud;
import cajeros.main;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import java.awt.Color;

public class DepositoCajero extends JFrame {

	private JPanel contentPane;
	private JTextField txtNumTarjeta1;
	private JTextField txtNip1;
	JLabel lblNewLabel,lblNewLabel_1;
	JButton btnEntrar;
	private JLabel lbNombre;
	private JLabel lbFondos;
	private static JButton btnDepositar;
	private static JButton btnRetirar;
	private static JButton btnSalir;
	private static JLabel lblNewLabel_3;
	private static JTextField txtEfectivo;
	private JButton btnregresar;
	private JTextPane txtpnCajeroBanquitoDe;
	
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DepositoCajero frame = new DepositoCajero();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public DepositoCajero() {
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 587, 415);
		contentPane = new JPanel();
		contentPane.setBackground(Color.ORANGE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
	    lblNewLabel = new JLabel("Num. Tarjeta:");
		lblNewLabel.setBounds(50, 125, 91, 14);
		contentPane.add(lblNewLabel);
		
		lblNewLabel_1 = new JLabel("Nip:");
		lblNewLabel_1.setBounds(35, 182, 73, 14);
		contentPane.add(lblNewLabel_1);
		
		txtNumTarjeta1 = new JTextField();
		txtNumTarjeta1.addMouseListener(new MouseAdapter() {
			
		});
		txtNumTarjeta1.setBounds(177, 122, 199, 20);
		contentPane.add(txtNumTarjeta1);
		txtNumTarjeta1.setColumns(10);
		
		txtNip1 = new JTextField();
		txtNip1.setBounds(135, 179, 86, 20);
		contentPane.add(txtNip1);
		txtNip1.setColumns(10);
		
		 btnEntrar = new JButton("Entrar");
		 btnEntrar.addActionListener(new ActionListener() {
		 	public void actionPerformed(ActionEvent arg0) {
		 	}
		 });
		btnEntrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				 lbNombre.setVisible(false);
				 lbFondos.setVisible(false);

			  funcionescrud metodos = new  funcionescrud();
				ClientesCajero user=new ClientesCajero();
				
				user.setNumero_tarjeta(Integer.parseInt(txtNumTarjeta1.getText()));
				user = metodos.seleccionarUsuario(user);
				int tarjeta=(Integer.parseInt(txtNumTarjeta1.getText()));
				int tarjeta1=user.getNumero_tarjeta();
				int pin=(Integer.parseInt(txtNip1.getText()));
				int pin1=user.getNip_tarjeta();
				Double  fondos=user.getSaldo_tarjeta();
				if(tarjeta == tarjeta1 && pin == pin1 ){
					JOptionPane.showMessageDialog(null, "Bienvenido: "+ user.getNombre_cliente(), " ", JOptionPane.INFORMATION_MESSAGE);
					lbNombre.setText(user.getNombre_cliente());
					lbFondos.setText("$"+fondos);
					Menuinicio();
				}else {
					
					JOptionPane.showMessageDialog(null, "El Nip de la tarjeta: "
					+ tarjeta1 + " \n Es incorrecto", getTitle(), JOptionPane.WARNING_MESSAGE);
				}
			}
			
		});
		btnEntrar.setBounds(155, 227, 89, 23);
		contentPane.add(btnEntrar);
		
		lbNombre = new JLabel("New label");
		lbNombre.setBounds(177, 125, 46, 14);
		contentPane.add(lbNombre);
		
		lbFondos = new JLabel("New label");
		lbFondos.setBounds(136, 182, 46, 14);
		contentPane.add(lbFondos);
		
		/* boton  que ara que pueda hacer el deposito*/ 
		btnDepositar = new JButton("Depositar");
		btnDepositar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				funcionescrud metodos = new  funcionescrud();
				ClientesCajero users=new ClientesCajero();
				
				users.setNumero_tarjeta(Integer.parseInt(txtNumTarjeta1.getText()));
				users= metodos.seleccionarUsuario(users);
				
				
				
				int id= (users.getIdUsuario());
				String nom=(users.getNombre_cliente());
				Double fondF=Double.parseDouble(txtEfectivo.getText());
				Double fond1=(users.getSaldo_tarjeta());
				int numTarjeta=(users.getNumero_tarjeta());
				int NIP=(users.getNip_tarjeta());
				Double fondI=fondF+fond1;
				
				metodos.actualizarRegistro(id, nom, fondI, numTarjeta, NIP);
				
				JOptionPane.showMessageDialog(null, "La cantidad depositada son: "
				+ fondF, getTitle(), JOptionPane.WARNING_MESSAGE);
			}
		});
		btnDepositar.setVisible(false);
		btnDepositar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnDepositar.setBounds(93, 306, 89, 23);
		contentPane.add(btnDepositar);
		/* boton de retirar cual ara la funcion del mismo */
		btnRetirar = new JButton("Retirar");
		btnRetirar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				funcionescrud metodos = new  funcionescrud();
				ClientesCajero users=new 	ClientesCajero();
				users.setNumero_tarjeta(Integer.parseInt(txtNumTarjeta1.getText()));
				users= metodos.seleccionarUsuario(users);
	/* datos del user */
				int id= (users.getIdUsuario());
				String nombre=(users.getNombre_cliente());
				Double fondosIntroducidos=Double.parseDouble(txtEfectivo.getText());
				Double fondoInicial=(users.getSaldo_tarjeta());
				int numTarjeta=(users.getNumero_tarjeta());
				int NIP=(users.getNip_tarjeta());
				/* creamos una validacion para retirar fondos del usuario */
				if(fondosIntroducidos<=fondoInicial) {
					Double fondTotal=fondoInicial-fondosIntroducidos;
					metodos.actualizarRegistro(id, nombre, fondTotal, numTarjeta, NIP);
					JOptionPane.showMessageDialog(null, "La cantidad a retirar es: "
					+ fondosIntroducidos, getTitle(), JOptionPane.WARNING_MESSAGE);
					System.out.print(fondTotal);
			/* validar si mi usuario de cajero cuenta con dinero suficiente*/	
				}else {
				
			JOptionPane.showMessageDialog(null, "No tienes mucho dinero"
				, "Ncesitas mas dinero", JOptionPane.WARNING_MESSAGE);
				}				
			}
		});
		btnRetirar.setVisible(false);
		btnRetirar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnRetirar.setBounds(268, 306, 89, 23);
		contentPane.add(btnRetirar);
		/* boton para salir de nuestra aplicacion */
		btnSalir = new JButton("Salir");
		btnSalir.setVisible(false);
		btnSalir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				main principal = new main();
				principal.setVisible(true);	
				
			}
		});
		btnSalir.setBounds(452, 306, 89, 23);
		contentPane.add(btnSalir);
		
		lblNewLabel_3 = new JLabel("Ingrese una Cantidad: ");
		lblNewLabel_3.setVisible(false);
		lblNewLabel_3.setBounds(12, 231, 129, 14);
		contentPane.add(lblNewLabel_3);
		
		txtEfectivo = new JTextField();
		txtEfectivo.setVisible(false);
		
		txtEfectivo.setBounds(279, 228, 86, 20);
		contentPane.add(txtEfectivo);
		txtEfectivo.setColumns(10);
		
		
		/* el boton que ara que nos mande al main  */
		btnregresar = new JButton("Regresar");
		btnregresar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				main principal = new main();
				principal.setVisible(true);	
			}
		});
		btnregresar.setBounds(411, 227, 89, 23);
		contentPane.add(btnregresar);
		
		txtpnCajeroBanquitoDe = new JTextPane();
		txtpnCajeroBanquitoDe.setText("cajero banquito de luis martin ceh balam ");
		txtpnCajeroBanquitoDe.setBounds(155, 55, 264, 22);
		contentPane.add(txtpnCajeroBanquitoDe);
	}
	
	public void Menuinicio() {
		txtNumTarjeta1.setVisible(false);
		txtNip1.setVisible(false);
		btnEntrar.setVisible(false);
		btnregresar.setVisible(false);
		
		btnSalir.setVisible(true);
		btnDepositar.setVisible(true);
		btnRetirar.setVisible(true);
		
		lblNewLabel_3.setVisible(true); 
		txtEfectivo.setVisible(true);
		
		lblNewLabel.setText("Nombre: ");
		lblNewLabel_1.setText("Fondos: ");
		lbNombre.setVisible(true);
		lbFondos.setVisible(true);
	}
	
}
