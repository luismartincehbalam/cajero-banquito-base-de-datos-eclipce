package cajeros;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JFormattedTextField;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
	
public class crearuser extends JFrame {

	private JPanel contentPane;
	private JTextField txtBienVenidoAdministrador;
	private JTextField textField;
	private JTextField textField_1;
	
	private JTextPane textnombre;
	private JTextPane textapellido ;
	private JTextPane texttarjeta;
	private JTextPane textnip;
	private JTextPane textsaldo;
	private JTextPane txtpnNombreUsuario;
	private JTextPane txtpnApellidoUsuario;
	private JTextPane txtpnNumeroTarjeta;
	private JTextPane txtpnSaldoTarjeta;
	private JTextPane txtpnNipTarjeta;
	private JTextField textid;
	private JTextField nombretext;
	private JTextField apellidotext;
	private JTextField numerotext;
	private JTextField niptext;
	private JTextField salfotext;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					crearuser frame = new crearuser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public crearuser() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 620, 484);
		contentPane = new JPanel();
		contentPane.setBackground(Color.ORANGE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtBienVenidoAdministrador = new JTextField();
		txtBienVenidoAdministrador.setText("Bien venido Administrador ");
		txtBienVenidoAdministrador.setBounds(212, 99, 166, 22);
		contentPane.add(txtBienVenidoAdministrador);
		txtBienVenidoAdministrador.setColumns(10);
		
		textField = new JTextField();
		textField.setText("DIA 26/02/2020");
		textField.setColumns(10);
		textField.setBounds(30, 13, 101, 22);
		contentPane.add(textField);
		
		textField_1 = new JTextField();
		textField_1.setText("Hora: 12:30 pm");
		textField_1.setColumns(10);
		textField_1.setBounds(469, 13, 116, 22);
		contentPane.add(textField_1);
		
		JTextPane txtpnId = new JTextPane();
		txtpnId.setText("ID :");
		txtpnId.setBounds(136, 175, 38, 22);
		contentPane.add(txtpnId);
		
		txtpnNombreUsuario = new JTextPane();
		txtpnNombreUsuario.setText("Nombre usuario:");
		txtpnNombreUsuario.setBounds(136, 210, 116, 22);
		contentPane.add(txtpnNombreUsuario);
		
		txtpnApellidoUsuario = new JTextPane();
		txtpnApellidoUsuario.setText("Apellido usuario:");
		txtpnApellidoUsuario.setBounds(136, 241, 116, 22);
		contentPane.add(txtpnApellidoUsuario);
		
		txtpnNumeroTarjeta = new JTextPane();
		txtpnNumeroTarjeta.setText("Numero tarjeta:");
		txtpnNumeroTarjeta.setBounds(136, 276, 106, 22);
		contentPane.add(txtpnNumeroTarjeta);
		
		txtpnSaldoTarjeta = new JTextPane();
		txtpnSaldoTarjeta.setText("Saldo tarjeta:");
		txtpnSaldoTarjeta.setBounds(136, 348, 93, 22);
		contentPane.add(txtpnSaldoTarjeta);
		
		txtpnNipTarjeta = new JTextPane();
		txtpnNipTarjeta.setText("Nip tarjeta:");
		txtpnNipTarjeta.setBounds(136, 313, 81, 22);
		contentPane.add(txtpnNipTarjeta);
		
		JButton btnguardar = new JButton("Guardar usuario");
		btnguardar.addMouseListener(new MouseAdapter() {
			/* indicar el valor de los text file para mandarlos a la clase clientes */
			@Override
			public void mouseClicked(MouseEvent arg0) {
				ClientesCajero NombreVariable =new ClientesCajero();
				funcionescrud Funcion=new  funcionescrud();
				NombreVariable.setIdUsuario(Integer.parseInt(textid.getText()));
				NombreVariable.setNombre_cliente((nombretext.getText()));
				NombreVariable.setApellidos_clientes((apellidotext.getText()));
				NombreVariable.setNumero_tarjeta(Integer.parseInt(numerotext.getText()));
				NombreVariable.setNip_tarjeta(Integer.parseInt(niptext.getText()));
				NombreVariable.setSaldo_tarjeta(Integer.parseInt(salfotext.getText()));
				Funcion.instertardatos(NombreVariable);  
				System.out.println("si se metieron");
			}
		});
		btnguardar.setBackground(Color.WHITE);
		btnguardar.setBounds(136, 402, 140, 22);
		contentPane.add(btnguardar);
		
		JButton btnir = new JButton("Ir a casa ");
		btnir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				main principal = new main();
				principal.setVisible(true);
			}
		});
		btnir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnir.setBackground(Color.WHITE);
		btnir.setBounds(336, 401, 97, 25);
		contentPane.add(btnir);
		
		textid = new JTextField();
		textid.setBounds(326, 175, 131, 22);
		contentPane.add(textid);
		textid.setColumns(10);
		
		nombretext = new JTextField();
		nombretext.setBounds(326, 210, 131, 22);
		contentPane.add(nombretext);
		nombretext.setColumns(10);
		
		apellidotext = new JTextField();
		apellidotext.setBounds(326, 241, 131, 22);
		contentPane.add(apellidotext);
		apellidotext.setColumns(10);
		
		numerotext = new JTextField();
		numerotext.setBounds(326, 276, 131, 22);
		contentPane.add(numerotext);
		numerotext.setColumns(10);
		
		niptext = new JTextField();
		niptext.setBounds(326, 313, 131, 22);
		contentPane.add(niptext);
		niptext.setColumns(10);
		
		salfotext = new JTextField();
		salfotext.setBounds(326, 348, 131, 22);
		contentPane.add(salfotext);
		salfotext.setColumns(10);
	}
}
