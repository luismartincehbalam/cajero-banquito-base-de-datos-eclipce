package cajeros;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Cajero.DepositoCajero;

import java.awt.Color;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class main extends JFrame {

	private JPanel contentPane;
	private JTextField txtBienVenidoA;
	private JTextField txtqueDeseasHacer;
	private JTextField txtDia;
	private JTextField txtHoraPm;
	private JButton btnConsultaTuUsuario;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					main frame = new main();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public main() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 603, 493);
		contentPane = new JPanel();
		contentPane.setBackground(Color.ORANGE);
		contentPane.setForeground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		txtBienVenidoA = new JTextField();
		txtBienVenidoA.setText("Bien venido a mi banquito de luis martin ceh balam :3");
		txtBienVenidoA.setBounds(124, 63, 317, 22);
		contentPane.add(txtBienVenidoA);
		txtBienVenidoA.setColumns(10);
		
		txtqueDeseasHacer = new JTextField();
		txtqueDeseasHacer.setText("\u00BFQue deseas hacer el dia de hoy?");
		txtqueDeseasHacer.setBounds(188, 117, 202, 22);
		contentPane.add(txtqueDeseasHacer);
		txtqueDeseasHacer.setColumns(10);
		
		txtDia = new JTextField();
		txtDia.setText("DIA 26/02/2020");
		txtDia.setBounds(24, 13, 101, 22);
		contentPane.add(txtDia);
		txtDia.setColumns(10);
		
		txtHoraPm = new JTextField();
		txtHoraPm.setText("Hora: 12:30 pm");
		txtHoraPm.setBounds(426, 13, 116, 22);
		contentPane.add(txtHoraPm);
		txtHoraPm.setColumns(10);
		
		JButton btnadmin = new JButton("Ir a Panel de administrador ");
		btnadmin.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			crearuser vista = new crearuser();
			vista.setVisible(true);
			}
		});
		btnadmin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnadmin.setForeground(Color.BLACK);
		btnadmin.setBackground(Color.WHITE);
		btnadmin.setBounds(200, 181, 183, 25);
		contentPane.add(btnadmin);
		
		JButton btncajero = new JButton("Ir a Funciones de cajero ");
		btncajero.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				DepositoCajero vistas = new  DepositoCajero();
				vistas.setVisible(true);
			}
		});
		btncajero.setBackground(Color.WHITE);
		btncajero.setBounds(188, 254, 202, 25);
		contentPane.add(btncajero);
		
		JButton btnCrearUsuario = new JButton("Crear usuario");
		btnCrearUsuario.setBackground(Color.WHITE);
		btnCrearUsuario.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
			crearuser vistass = new crearuser();
			vistass.setVisible(true);
			}
		});
		btnCrearUsuario.setBounds(220, 311, 138, 25);
		contentPane.add(btnCrearUsuario);
		
		btnConsultaTuUsuario = new JButton("Consulta tu usuario");
		btnConsultaTuUsuario.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				BuscarUser vistasss = new BuscarUser();
				vistasss.setVisible(true);
			}
		});
		btnConsultaTuUsuario.setBackground(Color.WHITE);
		btnConsultaTuUsuario.setBounds(209, 374, 149, 25);
		contentPane.add(btnConsultaTuUsuario);
	}
}
