package cajeros;



public class ClientesCajero {
	
	 private int idUsuario;	
	 private String nombre_cliente;
	 private String apellidos_clientes;
	 private int numero_tarjeta;
	 private Double saldo_tarjeta;
	 private int nip_tarjeta;
	 
	public int getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getNombre_cliente() {
		return nombre_cliente;
	}
	public void setNombre_cliente(String nombre_cliente) {
		this.nombre_cliente = nombre_cliente;
	}
	public String getApellidos_clientes() {
		return apellidos_clientes;
	}
	public void setApellidos_clientes(String apellidos_clientes) {
		this.apellidos_clientes = apellidos_clientes;
	}
	public int getNumero_tarjeta() {
		return numero_tarjeta;
	}
	public void setNumero_tarjeta(int numero_tarjeta) {
		this.numero_tarjeta = numero_tarjeta;
	}
	public Double getSaldo_tarjeta() {
		return saldo_tarjeta;
	}
	public void setSaldo_tarjeta(double saldo_tarjeta) {
		this.saldo_tarjeta =  saldo_tarjeta;
	}
	public int getNip_tarjeta() {
		return nip_tarjeta;
	}
	public void setNip_tarjeta(int nip_tarjeta) {
		this.nip_tarjeta = nip_tarjeta;
	}
	
@Override
 public String toString() {
	return "clientes"
			+ ": \n"+"idUsuario="+idUsuario+",\n nombre_cliente="+nombre_cliente+": \n"
			+ "\n apellidos_clientes="+apellidos_clientes+ "\n numero_tarjeta"+numero_tarjeta+"\n saldo_tarjeta="
			+saldo_tarjeta + "\n nip_tarjeta ="+nip_tarjeta;
}
}

