package cajeros;

import java.util.ArrayList;
import java.util.List;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;

import com.db4o.ObjectSet;


public class funcionescrud {

	private ObjectContainer Batabase;

	/* abrir conexion de base de datos */
	private void conexion() {
		
		Batabase=Db4oEmbedded.openFile("banquito");
    }
	/* cerrar la conexion de la base de datos */
	private void conexioncerrada() {
		
		Batabase.close();
		
	}
	/* insertar un registro  importante abrir y cerrar conexion*/
	void instertardatos(ClientesCajero Clientes) {
		conexion();
		Batabase.store(Clientes);
		conexioncerrada();
	}
	
	public ClientesCajero seleccionarUsuario(ClientesCajero usuarios) {
		 conexion();
		ObjectSet resultado=Batabase.queryByExample(usuarios);
		ClientesCajero usuario=(ClientesCajero) resultado.next();
		conexioncerrada();
		return usuario;
		
	}
	/* metodos de actualizar prueba */

		public void actualizarRegistro(int id_user,String nombre_user,Double fondo_user,int numtarjeta_user, int nip_user) {

			conexion();
			ClientesCajero p = new ClientesCajero();
			p.setNumero_tarjeta(numtarjeta_user);
			ObjectSet resultado=Batabase.queryByExample(p);
			ClientesCajero auxiliar=(ClientesCajero) resultado.next();
			auxiliar.setIdUsuario(id_user);
			auxiliar.setNombre_cliente(nombre_user);
			auxiliar.setSaldo_tarjeta(fondo_user);
			auxiliar.setNumero_tarjeta(numtarjeta_user);
			auxiliar.setNip_tarjeta(nip_user);
			Batabase.store(auxiliar);
			conexioncerrada();
		}
		public void setVisible(boolean b) {
			
		}
}
