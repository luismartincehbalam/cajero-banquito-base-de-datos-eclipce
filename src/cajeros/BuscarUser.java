package cajeros;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Cajero.DepositoCajero;

import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import javax.swing.JTextPane;
import cajeros.ClientesCajero;
import cajeros.funcionescrud;

public class BuscarUser extends JFrame {

	private JPanel contentPane;
	private JTextField txtID;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BuscarUser frame = new BuscarUser();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public BuscarUser() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.ORANGE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Consultar");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				
			    funcionescrud metodos=new  funcionescrud();
				ClientesCajero usuario=new ClientesCajero();
				usuario.setNumero_tarjeta(Integer.parseInt(txtID.getText()));
				usuario = metodos.seleccionarUsuario(usuario);
				
JOptionPane.showMessageDialog(null, "Los datos seleccionados son: \n "+ usuario, getTitle(), JOptionPane.WARNING_MESSAGE);

			
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(65, 200, 89, 23);
		contentPane.add(btnNewButton);
		
		txtID = new JTextField();
		txtID.setBounds(271, 135, 86, 20);
		contentPane.add(txtID);
		txtID.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("NumTarjeta usuario:");
		lblNewLabel.setBounds(34, 138, 149, 14);
		contentPane.add(lblNewLabel);
		
		JButton btnregresar = new JButton("Atras");
		btnregresar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
			
				main principal = new main();
				principal.setVisible(true);
				
			}
		});
		btnregresar.setBounds(268, 200, 89, 23);
		contentPane.add(btnregresar);
		
		JTextPane txtpnQueUsuarioDeseas = new JTextPane();
		txtpnQueUsuarioDeseas.setText("Que usuario deseas buscar en banquito ");
		txtpnQueUsuarioDeseas.setBounds(92, 63, 236, 22);
		contentPane.add(txtpnQueUsuarioDeseas);
	}
}
